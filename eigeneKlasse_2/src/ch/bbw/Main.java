package ch.bbw;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	Triangle triangle = new Triangle();
    triangle.draw();

    //Variant1
        triangle.setHeight(9);
        triangle.draw();

    //Variant2
        Scanner keyboardInput = new Scanner(System.in);
        System.out.println("Geben Sie die Dreieckshöhe ein");
        int height = keyboardInput.nextInt();
        triangle.setHeight(height);
        triangle.draw();

    }
}
