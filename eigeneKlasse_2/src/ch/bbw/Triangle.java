package ch.bbw;

public class Triangle {
    private int height = 8;


    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void draw() {
        int width = 1;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                System.out.print("*");}
            width++;
            System.out.println();
        }
    }
}
